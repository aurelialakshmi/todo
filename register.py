from flask import Flask, request, logging, jsonify
from flask_mysqldb import MySQL
from passlib.hash import sha256_crypt

app = Flask(__name__)

app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'aurelial7'
app.config['MYSQL_DB'] = 'myflaskapp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)


@app.route('/register', methods=['POST'])
def register():
    result = {'success': False}
    admin_choices = ['1', '0']
    try:
        try:
            name = request.form['name']
            email = request.form['email']
            username = request.form['username']
            admin = request.form['admin']
            password = sha256_crypt.encrypt(str(request.form['password']))
        except Exception as e:
            print(e)
            result.update({'success': False, 'message': 'Name, email, username, admin, password fields are mandatory'})
            return jsonify(result)
        if len(admin) > 0 and len(name) > 0 and len(username) > 0 and len(email) > 0 and len(password) > 0:
            if admin in admin_choices:
                admin = 1 if admin is '1' else 0
                app.logger.info(admin)
                cur = mysql.connection.cursor()
                cur.execute("Select username from users where username=%s",[username])
                user_named = cur.fetchall()
                if user_named:
                    result.update({'success': False, 'message': 'Username you added already exists, add another'})
                else:
                    cur.execute("INSERT INTO users(name, email, username, admin, password) VALUES(%s, %s, %s, %s, %s)",
                                (name, email, username, admin, password))
                    mysql.connection.commit()
                    cur.close()
                    result.update({'success': True, 'message': 'User added successfully. You can now login'})
            else:
                result.update(
                    {'success': False, 'message': 'Admin value should be either 1 or 0'})
        else:
            result.update({'success': False, 'message': 'Name, email, username, admin, password fields are mandatory'})
    except Exception as e:
        print(e)
        result.update({'success':False, 'message': 'Something went wrong'})
    return jsonify(result)


if __name__ == '__main__':
    app.run(debug=True)