from datetime import datetime, date
from flask import Flask, flash, session, request, logging, jsonify
from flask_mysqldb import MySQL
from passlib.hash import sha256_crypt
from functools import wraps

app = Flask(__name__)

app.config['MYSQL_HOST'] = '127.0.0.1'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'aurelial7'
app.config['MYSQL_DB'] = 'myflaskapp'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

# init MYSQL
mysql = MySQL(app)

status_types = ['not started', 'finished', 'in progress']


@app.route('/')
def index():
    return jsonify({'message':'You are at the index of Todo App'})


@app.route('/login',  methods=['POST'])
def login():
    result = {'success': False}
    try:
        username = request.form['username']
        password_candidate = request.form['password']
        cur = mysql.connection.cursor()
        user = cur.execute("SELECT * FROM users WHERE username = %s",[username])
        if user > 0:
            data = cur.fetchone()
            password = data['password']
            admin = data['admin']

            if admin:
                session['admin'] = True
                app.logger.info(admin)
            if sha256_crypt.verify(password_candidate, password):
                session['logged_in'] = True
                session['username'] = username
                result.update({'success': True, 'message':'Hi ' + username + '! You are now logged in'})
            else:
                result.update({'success': False, 'message': 'Invalid login'})
            cur.close()
        else:
            result.update({'success': False, 'message': 'Username not found'})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': str(e)})
    return jsonify(result)


def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return jsonify('You are not logged in. You need to login to view this page.')
    return wrap


def admin(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'admin' in session:
            return f(*args, **kwargs)
        else:
            return jsonify('You are not an admin. Only admins have access to this page.')
    return wrap


@app.route('/add_task', methods=['POST'])
@is_logged_in
@admin
def add_task():
    result = {'success': False}
    try:
        task_name = request.form['task_name']
        app.logger.info(task_name)
        due_date = request.form['due_date']
        try:
            datetime.strptime(due_date, '%Y-%m-%d')
        except Exception as e:
            print(e)
            result.update({'success': False, 'message': 'Please enter date with format YYYY-MM-DD'})
            return jsonify(result)
        status = request.form['status']
        if status.lower() in status_types:
            cur = mysql.connection.cursor()
            task_named = cur.execute("SELECT * FROM tasks where task_name =%s",[task_name])
            if task_named:
                result.update({'success': False, 'message': 'Task with entered name already exists. Enter another name'})
            else:
                cur.execute("INSERT INTO tasks(task_name, due_date, status) VALUES(%s, %s, %s)", (task_name, due_date, status))
                mysql.connection.commit()
                cur.close()
                result.update({'success': True, 'message': 'Added successfully'})
        else:
            result.update({'success': False, 'message': 'Status type is not valid.'})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong.'})
    return jsonify(result)


@app.route('/tasks', methods=['GET'])
@is_logged_in
def tasks():
    result = {'success': False}
    try:
        cur = mysql.connection.cursor()
        count_of_tasks = cur.execute("SELECT * FROM tasks")
        tasks_here = cur.fetchall()
        if count_of_tasks > 0:
            result.update({'success': True,'tasks': tasks_here})
        else:
            result.update({'success': True,'msg': 'No tasks found'})
        cur.close()
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong'})
    return jsonify(result)


@app.route('/edit_task/<string:id>', methods=['POST'])
@is_logged_in
@admin
def edit_task(id):
    result = {'success': False}
    try:
        task_name = request.form['task_name']
        due_date = request.form['due_date']
        try:
            datetime.strptime(due_date, '%Y-%m-%d')
        except Exception as e:
            print(e)
            result.update({'success': False, 'message': 'Please enter date with format YYYY-MM-DD'})
            return jsonify(result)
        status = request.form['status']
        if status.lower() in status_types:
            cur = mysql.connection.cursor()
            cur.execute("UPDATE tasks SET task_name=%s, due_date=%s, status=%s WHERE id=%s",
                        (task_name, due_date, status, id))
            mysql.connection.commit()
            cur.close()
            result.update({'success': True, 'message': 'Task edited successfully'})
        else:
            result.update({'success': False, 'message': 'Status type is not valid'})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong'})
    return jsonify(result)


@app.route('/due', methods=['GET'])
@is_logged_in
def due():
    result = {'success': False}
    try:
        date = request.args.get('due_date')
        try:
            datetime.strptime(date, '%Y-%m-%d')
        except Exception as e:
            print (e)
            result.update({'success': False, 'message': 'Please enter date with format YYYY-MM-DD'})
            return jsonify(result)
        cur = mysql.connection.cursor()
        status_here = 'Finished'
        cur.execute("SELECT * from tasks WHERE status !=%s and due_date=%s", [status_here, date])
        tasks = cur.fetchall()
        cur.close()
        if tasks:
            result.update({'success': True,'tasks': tasks})
        else:
            result.update({'success': True, 'message': 'No tasks were found on the date entered : '+date})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong'})
    return jsonify(result)


@app.route('/overdue', methods=['GET'])
@is_logged_in
def overdue():
    result = {'success': False}
    try:
        cur = mysql.connection.cursor()
        status_here = 'Finished'
        today = date.today()
        cur.execute("SELECT * from tasks WHERE status !=%s and due_date<%s",[status_here, today])
        tasks = cur.fetchall()
        cur.close()
        if tasks:
            result.update({'success': True,'tasks': tasks})
        else:
            result.update({'success': True, 'tasks': 'No tasks were found'})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong'})
    return jsonify(result)


@app.route('/finished',  methods=['GET'])
@is_logged_in
def finished():
    result = {'success': False}
    try:
        cur = mysql.connection.cursor()
        status_here = 'Finished'
        cur.execute("SELECT * from tasks WHERE status =%s", [status_here])
        tasks = cur.fetchall()
        cur.close()
        if tasks:
            result.update({'success': True,'tasks': tasks})
        else:
            result.update({'success': True, 'tasks': 'No tasks were found'})
    except Exception as e:
        print(e)
        result.update({'success': False, 'message': 'Oops! Something went wrong'})
    return jsonify(result)


@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    return jsonify('You are logged out')


if __name__ == '__main__':
    app.secret_key = "secret123"
    app.run(debug=True)